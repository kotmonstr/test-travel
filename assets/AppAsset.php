<?php

namespace micro\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@micro';
    public $baseUrl = '@web';
    public $css = [
        //'css/bootstrap.css',
        'css/style.css',
    ];
    public $js = [
        'js/menu.js'
    ];
    public $depends = [
        //'yii\web\YiiAsset',

    ];
}
