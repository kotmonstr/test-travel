<?php
use yii\helpers\Url;
?>

<header>
    <div class="header__logo"><h3>Резюме</h3><p>Сычёв Константин</p></div>

    <nav>
        <div class="topnav" id="myTopnav">
            <a href="<?= Url::to('/') ?>">Главная</a>
            <a href="<?= Url::to('/site/about') ?>">Обо мне</a>
            <a href="#">Услуги</a>
            <a href="#">Работы</a>
            <a href="#">Отзывы</a>
            <a href="#">Контакты</a>
            <a href="#" id="menu" class="icon">&#9776</a>
        </div>
    </nav>
</header>