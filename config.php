<?php
return [
    'id' => 'test.loc',
    // basePath (базовый путь) приложения будет каталог `micro-app`
    'basePath' => __DIR__,
    // это пространство имен где приложение будет искать все контроллеры
    'controllerNamespace' => 'micro\controllers',
    // установим псевдоним '@micro', чтобы включить автозагрузку классов из пространства имен 'micro',
    'aliases' => [
        '@micro' => __DIR__,
        '@web'=> __DIR__.'/web'
    ],

    'components' => [
        'request' => [
            'cookieValidationKey' => 'kotmonstr',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                   '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
    ],

];